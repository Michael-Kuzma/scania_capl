CAPL keyboard controls

'a' = enable accel
's' = increment accel 1%
'd' = decrement accel 1%

'b' = enable brake
'n' = increment brake 1%
'm' = decrement brake 1%

't' = enable shift
'y' = set gear to reverse
'u' = set gear to neutral
'i' = set gear to drive